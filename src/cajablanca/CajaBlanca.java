/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cajablanca;

/**
 *
 * @author nacho
 */
public class CajaBlanca {

    public static void main(String[] args) {
        /*String cadena="a";
        char letra='b';
        System.out.println(hayMayorCadena(cadena,letra));*/
        /*String notaPracticas="APTO";
        double notaTeoria=10;
        double notaTrabajos=2;
        System.out.println(obtenerNotaFinal(notaTeoria, notaPracticas, notaTrabajos));*/
        System.out.println(buscarEn("amapalaaaaa",'a'));
    }
    
    // Devuelve true si algún caracter de los que componen la
    // cadena "cadena" es mayor estrictamente que el caracter
    // "letra"
    public static boolean hayMayorCadena(String cadena, char letra) {
        boolean encontrado=false;
        int longitud=cadena.length();
        int i=0;
        while (!encontrado && i<longitud) {
            if (cadena.charAt(i)>letra) {
                encontrado=true;
            }
            i++;
        }
        return encontrado;
    }
    
    // notaPracticas puede ser "APTO" o "NO APTO"
    // notaTrabajos puede ser de 0 a 2
    // notaTeoria de 0 a 10
    public static String obtenerNotaFinal(double notaTeoria, String notaPracticas, double notaTrabajos) {
        String notaFinal;                           // 1
        if (notaPracticas.equalsIgnoreCase("NO APTO")) {                 // P2
            notaFinal="4";                              // 3
        } else {
            if (notaTeoria<4.5) {                       // P4
                notaFinal=notaTeoria+"";                // 5
            } else {
                notaFinal=notaTeoria+notaTrabajos+"";   // 6
                
            }
            if (Double.parseDouble(notaFinal)>10) {     // P7
                notaFinal="Matricula Honor";            // 8
            }
        }
        return notaFinal;                               // 9
    }
    // Devuelve el número de apariciones de la letra en la cadena
    public static int buscarEn(String cadena, char letra) {
        int contador=0;
        int n=0;
        int lon = cadena.length();
        if (lon>0) {
            do {
                if (cadena.charAt(contador)==letra) {
                    n++;
                }
                contador++;
                lon--;
            } while (lon>0);
        }
        return n;
    }
}
